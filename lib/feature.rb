require 'yaml'

module Gitlab
  module Homepage
    class Feature
      def initialize(data)
        @data = data
      end

      ##
      # Accessor for serialized tier hash
      #
      def [](key)
        @data[key.to_s]
      end

      ##
      # Middeman Data File objects compatibiltiy
      #
      def method_missing(name, *args, &block) # rubocop:disable Style/MethodMissing
        @data[name.to_s]
      end

      ##
      # Test for a feature with missing support in GitLab
      #
      def missing?
        !(gitlab_core || gitlab_starter || gitlab_premium || gitlab_ultimate)
      end

      def screenshot?
        screenshot_url != nil
      end

      def self.all!
        @features ||= YAML.load_file('data/features.yml')
        @features['features'].map do |data|
          new(data)
        end
      end

      def self.for_category(category)
        all!.dup.keep_if { |feature| feature&.category&.include?(category.key) }
      end
    end
  end
end

require 'yaml'

module Gitlab
  module Homepage
    class Stage
      attr_reader :key

      def initialize(key, data)
        @key = key
        @data = data
      end

      def categories
        @categories ||= Category.for_stage(self)
      end

      def competitors
        @competitors ||= Competitor.for_stage(self)
      end

      ##
      # Middeman Data File objects compatibiltiy
      #
      def method_missing(name, *args, &block) # rubocop:disable Style/MethodMissing
        @data[name.to_s]
      end

      def self.all!
        @stage_data ||= YAML.load_file('data/stages.yml')
        @stage_data['stages'].map do |key, data|
          new(key, data)
        end
      end
    end
  end
end

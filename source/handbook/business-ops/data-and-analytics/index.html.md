---
layout: markdown_page
title: "Data & Analytics"
---

## On this page
{:.no_toc}

- TOC
{:toc}

Primary project https://gitlab.com/meltano/analytics/

Looker project https://gitlab.com/meltano/looker/

## Extract and Load

The data team is the first customer of [Meltano](https://about.gitlab.com/handbook/meltano/). Wherever possible, we rely on Meltano for extractors and loaders.  

### Using SheetLoad

SheetLoad is the process by which a GoogleSheet can be ingested into the data warehouse. This is not an ideal solution to get data into the warehouse, but may be the appropriate solution at times. 

How to use SheetLoad

1. Add file to [SheetLoad Google Drive Folder](https://drive.google.com/drive/u/0/folders/1F5jKClNEsQstngbrh3UYVzoHAqPTf-l0) with appropriate naming convention, described in detail below
2. Share the sheet with with the SheetLoader runner => Doc with email ([GitLab internal](https://docs.google.com/document/d/1m8kky3DPv2yvH63W4NDYFURrhUwRiMKHI-himxn1r7k/edit))
3. Add the full file name to the [`extract-ci.yml`](https://gitlab.com/meltano/analytics/blob/master/extract/extract-ci.yml#L90) file
4. Create dbt base models
5. Add to [data quality test](https://gitlab.com/meltano/analytics/tree/master/transform/cloudsql-dbt/tests) that helps ensure these files are updated monthly. 

## Orchestration

We are in the process of moving from GitLab CI to Airflow.

## Database

We are in the process of moving from CloudSQL to Snowflake.

## Transformation- dbt

* Getting started with dbt
* Rules


## Visualization- Looker

* Getting Looker Access
* Using Looker (Coming from Redash)
* Using Looker (New)
* Developing in Looker
   * When making a change to an xf model, use sets

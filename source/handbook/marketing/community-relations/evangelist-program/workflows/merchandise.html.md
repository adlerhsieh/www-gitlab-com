---
layout: markdown_page
title: "Merchandise workflow"
---

## On this page
{:.no_toc}

- TOC
{:toc}

----

## Overview

## Workflow

## Best practices

### Choosing a vendor

As a general rule, consider using [Stickermule](https://www.stickermule.com) for sending stickers, since the Printfection inventory is limited. If Stickermule doesn't work for you, then use Printfection instead.

If the merch shipment includes:
* only stickers, always use Stickermule
* a small number of items (depending on Printfection inventory) use Printfection
* a large amount of stickers and other merch, consider using both Stickermule and Printfection

<i class="fas fa-hand-point-right" aria-hidden="true" style="color: rgb(138, 109, 59)
;"></i> Always check the Printfection inventory and item availability before sending.
{: .alert .alert-warning}

## Automation

---

## Handling swag

### MVP Appreciation Gifts

Each 22nd of the month is a release day - every release we pick a Most Valuable Person and thank them for their contributions. We send them some GitLab swag as a thank you (e.g. a hoodie, socks, and a handmade tanuki). There's also the option of sending personalized swag - see [custom swag providers](#good-custom-swag-providers).

1. Determine MVP after merge window closes, see `#release-mvp` channel
1. Find MVP's contact information
  * An email address is usually stored in git commit data
  * A user might have email or twitter info on their profile
1. Congratulate the MVP via email, ask for their shipping address, as well as any other relevant information (e.g. shirt size)
1. Investigate the MVP's interests
  * If the MVP doesn't have a notable presence on social media, you may choose to ask them directly or send GitLab swag instead
1. Choose a suitable gift (up to 200$ USD)
1. Write a kind thank you message
1. Send the gift
  * The MVP should ideally have the gift 48h before the post goes live, though shipping to people outside the United States can take longer and usually won't make it in time
1. Verify shipment status
  * Make sure that it was sent
  * Make sure that it arrived
1. Mention the MVP gift in the release post
  * Make sure there's a picture of the gift in the release post if it's available

### Handling #swag channel requests

Handle swag requests made in the #swag channel.

### Requests for customer events or conferences

Ask in the #swag Slack Channel and ping the [swag expert](/expertises) or any Community Advocate available.

Please include the following in your request:
* The expected number of event guests
* The merchandise shipping address and contact phone number
* Which merchandise items you need

Note: we recommend that you request merchandise at least 4 weeks in advance for us to be able to accommodate your request. However,
* If your request is urgent, please reach out to the swag expert and find out if the fast shipping option is available.
* Feel free to schedule a Zoom call with the swag expert to discuss, create and place the order.


### Community swag requests

* Email your request to merch@gitlab.com. In your request please include the expected number of guests, the best shipping address, and phone number along with what kind of swag you are hoping for. The swag we have available can be found on our online store. Note: We recommend you request swag at least 4 weeks out from the event date or we may not be able to accommodate your request.
* We get a lot of requests to send swag, and we try to respond to them all. If you would like to get some GitLab swag for your team or happening, please see below for more info on submitting a swag. Note: We recommend you request swag at least 4 weeks out from the event date or we may not be able to accommodate your request.

#### Requester

Here's the process for requesting a swag gift for a contributor/user/customer:

* Leave a message in the `#swag` channel with
  * URL to blog post, tweet, etc.
  * *(Optional)* Name
  * *(Optional)* Email
  * *(Optional)* Shipment Address
  * *(Optional)* Items requested (with sizes if you know them)

Request templates:

* Minimal
```plain
https://twitter.com/gitlab/status/884983979992121344
```

* Partial
```plain
https://twitter.com/gitlab/status/884983979992121344 - John Doe - email@example.com
```

* Maximum
```plain
https://twitter.com/gitlab/status/884983979992121344 - John Doe - email@example.com - 1233 Howard St 2F, CA, USA - 1 x L T-Shirt & 2 large stickers
```
<i class="fas fa-hand-point-right" aria-hidden="true" style="color: rgb(138, 109, 59);"></i> If you don't specify which swag to send, we'll send a standard package (T-Shirt + 2 stickers).
{: .alert .alert-warning}

<i class="fas fa-info-circle" aria-hidden="true" style="color: rgb(49, 112, 143)
;"></i> Please keep a single swag request confined to one message to avoid clutter.
{: .alert .alert-info}

#### Community Advocates

* Reach out to the swag recipient
  * Thank them for their work/support
  * Gather the missing info needed for fulfilling the swag dropship
* Fulfill the swag shipment in Printfection
  * If items are specified
    * Create a new dropship
    * Add items
    * Fill shipment info
  * If no items are specified
    * Take an unused giveaway link from [the community swag giveaway spreadsheet](https://docs.google.com/a/gitlab.com/spreadsheets/d/1LiRTSTHnF_NGyEPlqeRMUBi5cpffCHgMK8K0wAvVD4E/edit?usp=sharing)
    * Mark the link as redeemed by entering the date in the `Redeemed` column
* Notify the recipient that the dropship has been created or send them a giveaway link
* Comment to the requester in a thread, notifying that the swag request has been fulfilled

<i class="fas fa-hand-point-right" aria-hidden="true" style="color: rgb(138, 109, 59);"></i> Please bear in mind the [list of countries we do not do business in](/handbook/sales-process/images_sales_process/#export-control-classification-and-countries-we-do-not-do-business-in-).
{: .alert .alert-warning}

---
layout: markdown_page
title: "Education and Open Source"
---

## On this page
{:.no_toc}

- TOC
{:toc}

----

## Overview

Description of the workflows when handling Gold/Ultimate license requests for the Education or Open Source initiatives.

## Workflow

![Education channel workflow](/images/handbook/marketing/community-relations/education-oss-workflow.png)

### Step by step

1. All applications sent via the form on our website land on Salesforce
1. Log into Salesforce
1. Find the **Lead** in Salesforce
   - Search via e-mail
1. Edit information about the **Lead**
   - Fill in ***Company Name***, ***Billing Address*** and ***Product***
1. Convert **Lead** to **Opportunity**
   - **Opportunity name** - `CompanyName-NumberOfUsers Product Source [w/ Support]` (e.g. ABC University-100 Ultimate EDU or ABC University-100 Ultimate EDU w/ Support)
1. Turn off Reminder
1. Edit Potential Users
   - On Account page) -> Number of Users
1. Select **Opportunity** (hover over Opportunities)
1. Create a New **Quote**
   - Select quote template
     - Select EDU/OSS template
   - Sold to Contact and Bill to Contact are the Primary Contact
   - Start Date - Today
   - Initial Term and Renewal Term - 12 months
   - Turn off Auto Renew
   - Click Next
   - Add base product
     - Add appropriate product (Ultimate or Gold for EDU/OSS with or without support)
   - Click Next
   - Click Next
   - Quantity - Number of Users
1. Verify **Quote**
1. If the error appears, fix the contact's address format (or ask for more details if they're missing)
1. Generate PDF
   - Open Opportunity
   - Open Notes & Attachments
   - View -> Verify PDF
1. Open **Quote**
1. Send the **Quote** for signing
   - Click Sertifi eSign
   - Use an adequate mail template
   - Click Next
   - Select the Quote PDF for signing
   - Click Preview -> Verify quote
   - Click Send for Signature
   - Click return
1. Click on the opportunity link and click edit
   - Set type: New Business
   - Set close date: today
   - Set stage: 6-Awaiting Signature
   - Click Save
1. Submit for Approval
   - Type: New Business
   - Close Date: Today
   - Stage: 6-Awaiting Signature
   - Amount: 0.00 or the annual amount of the support (if it has support)
1. Extra steps for Gold subscriptions only (after it’s been approved)
   - Go to [customers portal](https://customers.gitlab.com/plans)
     - Credentials are in 1P
   - Have customer verify their namespace (Use template included below)
   - Upgrade them to Gold after it’s done

1. Once approved, Deal Desk will send the quote to Zuora
1. EULA will be sent and must be accepted
1. After EULA acceptance, license key will be delivered
1. A renewal opportunity will be created owned by the Community Advocate user

## Best practices

### Rejecting applications

* Go to the OSS/EDU campaign
* Open the lead object
* Set the lead object status to “Unqualified”
* Send them a templated rejection email

### Response templates

#### Namespace verification template

```
Information for GitLab.com Gold Upgrades: Groups must follow these instructions to authenticate.

Your subscription has been uploaded and you may follow these instructions to authenticate your groups:

1. Please visit https://customers.gitlab.com/customers/password/new to reset your account password
2. After logging in, please access the "Subscriptions" menu
3. You'll be able to click on "Edit" over a subscription
4. You'll be redirected to GitLab.com for OAuth login
5. At this point, you need to make sure you're logging in using the account you want to license on GitLab.com
6. Please select the Group you want to license then click onto "Update"

Please let me know if you have any questions, always happy to help.

Regards,
YOUR_NAME
```

#### Sertifi eSign e-mail template

```
Hello,

We’re excited to tell you that your application for the GitLab (EDU or OSS) program has been approved and processed.

We’re sending you a quote that you need to sign after which you’ll have to accept our EULA. Upon acceptance, you’ll get the license key delivered to you via mail (for Ultimate), or further instructions on how to authenticate your groups (for Gold).

Regards,
YOUR_NAME
```

#### Application rejection

##### Education

```
Hi,

Thanks for applying to GitLab’s EDU program.

Unfortunately, we couldn’t accept your application. Please refer to the eligibility requirements listed at https://about.gitlab.com/education.

A common reason for rejection is students applying on behalf of their University. We’re only able to accept applications submitted by University staff, as part of the process is signing a legal document on behalf of the University.

Another common reason is duplicate applications. We’re only able to accept one application per University. This isn’t to discriminate against smaller independent groups at universities, but rather to prevent the program from growing into unmaintainability.

Regards,
YOUR_NAME
```

##### Open Source

```
Hi,

Thanks for applying to GitLab’s OSS program.

Unfortunately, we couldn’t accept your application. Please refer to the eligibility requirements listed at https://gitlab.com/gitlab-com/gitlab-oss.

A common reason for rejection is the project not being licensed under one of the OSI approved licenses (see https://opensource.org/licenses/alphabetical for a list).

Regards,
YOUR_NAME
```

#### Troubleshooting

* All licenses are in [the license app](https://license.gitlab.com)

## Automation

TBD: this needs to be explained in more detail

Application form -> Marketo -> Salesforce -> Zendesk
